Describe some of the different types of layouts that are available on Android:
- LinearLayout: aligns all children in a single direction, vertically or horizontally
- ConstraintLayout: lay out child views using ‘constraints’ to define position based relationships between different views found in our layout
- FrameLayout: placeholder on screen that you can use to display a single view

What is a toast?
-  It's a small popup message displayed on the screen.

What does it mean to inflate a fragment?
- Show a specific layout within some ViewGroup (a viewgroup acts like a parent and can have children views)

What is the role of the R object?

- It helps identify the different parts of the layout/views that we created in the XML